'use strict'
const jwt = require('jsonwebtoken')

exports.DecodeToken = (req,res) => {
    let encToken = req.headers['authorization'].replace(/Bearer\s/, '')
    let decToken = jwt.decode(encToken)

    //check if token is inject or invalid
    if (decToken === null) return res.json({ msg: 'invalid token !' })

    //check if resource_access or audience not set for this service
    if (typeof decToken.resource_access['demo-client'] === "undefined") return false
    return decToken.resource_access['demo-client']
}